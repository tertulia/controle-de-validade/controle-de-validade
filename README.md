
# **Controle de Validade API**

  

Projeto utiliza [3.0.2](https://docs.djangoproject.com/en/3.0/) e [django rest framework](https://www.django-rest-framework.org)

  
  

## Clonar projeto para desenvolvimento

User o comando para clonar o projeto em seu computador

```

$ https://gitlab.com/tertulia/controle-de-validade/controle-de-validade.git

```

## Instalar dependências

### Ambiente virtual

Vá para o diretório do projeto

```

$ cd controle-de-validade

```

Para instalar o ambiente virtual use os comandos

```

$ pip install virtualenv

$ virtualenv venv -p python

```

Para ativar o ambiente virtual use o comando

- linux `source venv/bin/activate`

- windows `venv\Scripts\activate`

### Instalação das dependências

Para instalar as dependências use o comando

```

$ pip install -r environments/requirements-local.txt

```

  

## HEROKU
### Para utilizar Heroku localmente:
* Instale [heroku-cli](https://devcenter.heroku.com/articles/heroku-cli)
* Faça login na conta *Tertúlia* com o comando 
``` $ heroku login ```
* Para criar um superuser user o comando:
``` $ heroku run python3 manage.py createsuperuser```


### API
* Para acessar a página admin (depois de ter criado o superuser) utilize:
 ```https://controledevalidade.herokuapp.com/admin```

* Para olhar os endpoints
```https://controledevalidade.herokuapp.com/api```
* Consumir usuários
	* POST novo usuário (campos obrigatórios: name, email, password)
	```https://controledevalidade.herokuapp.com/api/accounts/register```
	* POST login (campos obrigatórios: email, password)
	```https://controledevalidade.herokuapp.com/api/accounts/login```
* Consumir produtos
	* GET todos os produtos
	```https://controledevalidade.herokuapp.com/api/products```
	* GET produto específico
	```https://controledevalidade.herokuapp.com/api/products/id```
	* POST novo produto (campos obrigatórios: name, image, description, acquisition_date, expiration_date, vouche_image, user_id)
	```https://controledevalidade.herokuapp.com/api/products/```
