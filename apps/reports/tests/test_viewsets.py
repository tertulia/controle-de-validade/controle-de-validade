from unittest import mock

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.test import APIClient

from apps.reports.models import Label, Report

REPORT_URL = reverse('reports-list')
label_URL = reverse('labels-list')


class TestPublicReportApi(TestCase):
    def test_not_allowed_methods(self):
        self.client = APIClient()

        get_response = self.client.get(REPORT_URL)
        self.assertEqual(get_response.status_code, status.HTTP_401_UNAUTHORIZED)

        post_response = self.client.post(REPORT_URL)
        self.assertEqual(post_response.status_code, status.HTTP_401_UNAUTHORIZED)

        put_response = self.client.put(REPORT_URL)
        self.assertEqual(put_response.status_code, status.HTTP_401_UNAUTHORIZED)

        del_response = self.client.delete(REPORT_URL)
        self.assertEqual(del_response.status_code, status.HTTP_401_UNAUTHORIZED)


class TestPrivateReportApi(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user('user@email.com', 'user name', 'pass')
        self.client = APIClient()
        self.client.force_authenticate(self.user)
        self.labels = [Label.objects.create(title=f'label1{i}') for i in range(3)]

        self.payload = dict(description="this is a report",
                            title="report title",
                            labels=[label.id for label in self.labels],
                            score=4)

        self.patcher = mock.patch('apps.reports.api.gitlab_api.requests.post')
        self.mock_request_post = self.patcher.start()

    def tearDown(self) -> None:
        self.patcher.stop()

    def test_un_allowed_methods(self):
        get_response = self.client.get(REPORT_URL)
        self.assertEqual(get_response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        put_response = self.client.put(REPORT_URL)
        self.assertEqual(put_response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        del_response = self.client.delete(REPORT_URL)
        self.assertEqual(del_response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_create_report_success(self):
        response = self.client.post(REPORT_URL, self.payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['description'], self.payload['description'])
        self.assertEqual(response.data['labels'], self.payload['labels'])
        self.assertEqual(response.data['score'], self.payload['score'])

    def test_create_report_fail(self):
        self.payload.pop('labels')

        response = self.client.post(REPORT_URL, self.payload)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_report_fail_no_send_issue_to_gitlab(self):
        self.payload.pop('labels')

        self.mock_request_post.return_value = Response({}, status=status.HTTP_201_CREATED)

        response = self.client.post(REPORT_URL, self.payload)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_send_issue_to_gitlab_success_change_reported_field(self):
        self.mock_request_post.return_value = Response({}, status=status.HTTP_201_CREATED)
        response = self.client.post(REPORT_URL, self.payload)
        report = Report.objects.get(title=self.payload['title'])

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(report.was_reported)

    def test_send_issue_to_gitlab_fail_no_change_reported_field(self):
        self.mock_request_post.return_value = Response({}, status=status.HTTP_400_BAD_REQUEST)
        response = self.client.post(REPORT_URL, self.payload)
        report = Report.objects.get(title=self.payload['title'])

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertFalse(report.was_reported)


class TestPubliclabelApi(TestCase):
    def test_not_allowed_methods(self):
        self.client = APIClient()

        get_response = self.client.get(label_URL)
        self.assertEqual(get_response.status_code, status.HTTP_401_UNAUTHORIZED)

        post_response = self.client.post(label_URL)
        self.assertEqual(post_response.status_code, status.HTTP_401_UNAUTHORIZED)

        put_response = self.client.put(label_URL)
        self.assertEqual(put_response.status_code, status.HTTP_401_UNAUTHORIZED)

        del_response = self.client.delete(label_URL)
        self.assertEqual(del_response.status_code, status.HTTP_401_UNAUTHORIZED)


class TestPrivatelabelApi(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user('user@email.com', 'user name', 'pass')
        self.client = APIClient()
        self.client.force_authenticate(self.user)
        self.labels = [
            Label.objects.create(title='t1'),
            Label.objects.create(title='t2'),
            Label.objects.create(title='t3'),
        ]

    def test_un_allowed_methods(self):
        post_response = self.client.post(label_URL)
        self.assertEqual(post_response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        put_response = self.client.put(label_URL)
        self.assertEqual(put_response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        del_response = self.client.delete(label_URL)
        self.assertEqual(del_response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_get_list_labels_success(self):
        response = self.client.get(label_URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data,
                         [{"title": t.title, "id": t.id} for t in self.labels])
