from django.contrib.auth import get_user_model
from django.test import TestCase

from apps.reports.api.serializers import LabelSerializer, ReportSerializer

from apps.reports.models import Label, Report


class TestLabelSerializer(TestCase):
    def setUp(self) -> None:
        self.label_params = {'title': 'a label title'}
        self.label = Label.objects.create(**self.label_params)

    def test_expected_fields(self):
        expected_fields = {'id', 'title'}
        serializer = LabelSerializer(self.label)

        self.assertEqual(set(serializer.data.keys()), expected_fields)

    def test_fields_content(self):
        serializer = LabelSerializer(self.label)
        self.label_params['id'] = self.label.id
        self.assertEqual(serializer.data, self.label_params)


class TestReportSerializer(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user('user@email.com', 'name', 'pass')
        self.label1 = Label.objects.create(title='label1')
        self.label2 = Label.objects.create(title='label2')
        self.label3 = Label.objects.create(title='label3')

        self.report_params = dict(
            description='a great report description',
            title='report title',
            informer=self.user,
            score=6
        )
        self.report = Report.objects.create(**self.report_params)
        self.report.labels.add(self.label1, self.label2, self.label3)

    def test_expected_field(self):
        serializer = ReportSerializer(self.report)
        expected_fields = {"informer", "description", "score", "labels", "title"}

        self.assertEqual(set(serializer.data.keys()), expected_fields)

    def test_field_content(self):
        self.report_params['labels'] = [self.label1.id, self.label2.id, self.label3.id]
        self.report_params['informer'] = self.user.name
        serializer = ReportSerializer(self.report)

        self.assertDictEqual(serializer.data, self.report_params)
