import datetime

from django.contrib.auth import get_user_model
from django.test import TestCase

from ..models import Label, Report


class TestLabelModel(TestCase):
    def test_label_representation(self):
        label = Label.objects.create(title='label title')
        self.assertEqual(str(label), label.title)


class TestReportModel(TestCase):
    def setUp(self) -> None:
        self.informer = get_user_model().objects.create_user(email='user@email.com', name='user name', password='pass')
        self.bug = Label.objects.create(title='bug')
        self.suggestion = Label.objects.create(title='suggestion')

    def test_create_report_success(self):
        report = Report.objects.create(informer=self.informer,
                                       description="Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                                       title='report title',
                                       score=10)
        report.labels.add(self.bug)
        report.labels.add(self.suggestion)

        self.assertEqual(report.creation_date, datetime.datetime.today().date())
        self.assertIn(self.bug, report.labels.all())
        self.assertIn(self.suggestion, report.labels.all())

    def test_report_representation(self):
        report = Report.objects.create(informer=self.informer,
                                       description="Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                                       score=10)
        self.assertEqual(str(report), report.title)

    def test_deletion_informer_no_effect_report(self):
        report = Report.objects.create(informer=self.informer,
                                       description="Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                                       score=10)
        self.informer.delete()

        self.assertTrue(Report.objects.filter(id=report.id).exists())

    def test_deletion_labels_no_effect_report(self):
        report = Report.objects.create(informer=self.informer,
                                       description="Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                                       score=10)
        report.labels.add(self.bug)
        report.labels.add(self.suggestion)

        self.bug.delete()
        self.suggestion.delete()

        self.assertTrue(Report.objects.filter(id=report.id).exists())
