from unittest import mock

from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework import status
from rest_framework.response import Response

from ..api.gitlab_api import ReportRepository
from ..models import Label, Report


class TestReportRepository(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user('user@email.com', 'name', 'password')
        self.labels = [
            Label.objects.create(title=f"label {i}") for i in range(3)
        ]
        self.report = Report.objects.create(informer=self.user, description="some thing", score=6)
        self.report.labels.add(*self.labels)
        self.report.save()
        self.report_repository = ReportRepository(self.report)

        self.mock_request = mock.patch('apps.reports.api.gitlab_api.requests.post', return_value=None)
        self.mock_post = self.mock_request.start()

    def tearDown(self) -> None:
        self.mock_request.stop()

    def test_report_repository_headers(self):
        expected_headers = {"Content-type", "PRIVATE-TOKEN"}
        self.assertSetEqual(expected_headers, set(self.report_repository.headers.keys()))

    def test_report_repository_fields(self):
        expected_fields = {"title", "description", "labels"}
        self.assertEqual(expected_fields, self.report_repository.fields.keys())

    def test_report_repository_project_id(self):
        project_id = '17779259'
        self.assertIn(project_id, self.report_repository.gitlab_api_url)

    def test_post_issue_success(self):
        self.mock_post.return_value = Response({'project_id': '17779259', 'title': self.report.title,
                                                'description': self.report.description,
                                                'labels': [label.title for label in self.labels]},
                                               status=status.HTTP_201_CREATED)

        report_repository = ReportRepository(self.report)
        response = report_repository.post()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
