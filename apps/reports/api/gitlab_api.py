import json

import requests
from decouple import config

from apps.reports.models import Report

PRIVATE_TOKEN = config('PRIVATE_TOKEN', default="")
GITLAB_PROJECT_ID = '17779259'
DEFAULT_LABELS = ['by user']


class ReportRepository:
    def __init__(self, report: Report):
        self.report = report

    @property
    def headers(self) -> dict:
        return {"Content-type": "application/json",
                "PRIVATE-TOKEN": PRIVATE_TOKEN
                }

    @property
    def gitlab_api_url(self) -> str:
        return f"https://gitlab.com/api/v4/projects/{GITLAB_PROJECT_ID}/issues"

    @property
    def fields(self) -> dict:
        return self._get_fields()

    def _get_fields(self) -> dict:
        return {"title": self.report.title,
                "description": self.report.description,
                "labels": [label.title for label in self.report.labels.all()] + DEFAULT_LABELS,
                }

    def post(self) -> requests.post:
        response = requests.post(self.gitlab_api_url, headers=self.headers, data=json.dumps(self.fields))
        return response
