from rest_framework import viewsets, mixins
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from .serializers import ReportSerializer, LabelSerializer
from ..models import Label, Report


class ReportViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin):
    authentication_classes = (TokenAuthentication,)
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = (IsAuthenticated, )

    def perform_create(self, serializer):
        serializer.save(informer=self.request.user)


class LabelViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    authentication_classes = (TokenAuthentication,)
    serializer_class = LabelSerializer
    queryset = Label.objects.all()
    permission_classes = (IsAuthenticated,)
