from rest_framework import serializers, status

from apps.reports.api.gitlab_api import ReportRepository
from apps.reports.models import Label, Report


class LabelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Label
        fields = ['id', 'title']

        extra_kwargs = {
            "id": {"read_only": True},
            "title": {"read_only": True},
        }


class ReportSerializer(serializers.ModelSerializer):
    informer = serializers.SerializerMethodField()

    class Meta:
        model = Report
        fields = ["title", "description", "informer", "score", "labels"]

    def get_informer(self, obj):
        return obj.informer.name

    def create(self, validated_data):
        labels = validated_data.pop('labels')
        instance = Report.objects.create(**validated_data)
        instance.labels.add(*labels)
        response = ReportRepository(instance).post()
        if response.status_code == status.HTTP_201_CREATED:
            instance.was_reported = True

        instance.save()
        return instance
