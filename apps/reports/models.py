from django.conf import settings
from django.db import models


class Label(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Report(models.Model):
    informer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    creation_date = models.DateField(auto_now_add=True)
    labels = models.ManyToManyField(Label)
    STATUS = [
        (0, 'closed'),
        (1, 'open')
    ]
    status = models.SmallIntegerField(choices=STATUS, default=1)
    score = models.SmallIntegerField()
    was_reported = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def short_description(self):
        return self.description[:10]
