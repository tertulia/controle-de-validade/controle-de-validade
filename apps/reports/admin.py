from django.contrib import admin

from .models import Label, Report

admin.site.register(Label)
admin.site.register(Report)
