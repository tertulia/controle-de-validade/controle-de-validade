from django import forms

from .models import Product


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'description', 'acquisition_date', 'expiration_date', 'image', 'voucher_image']
        labels = {"name": "Nome",
                  "image": "Imagem do produto",
                  "description": "Descrição",
                  "acquisition_date": "Data da compra",
                  "expiration_date": "Data de validade ou da garantia",
                  "voucher_image": "Imagem do comprovante ou da garantia"
                  }

        widgets = dict(
            name=forms.TextInput(attrs={"class": "form-control"}),
            description=forms.TextInput(attrs={"class": "form-control"}),
            image=forms.FileInput(attrs={"class": "btn btn-fill btn-primary"}),
            voucher_image=forms.FileInput(attrs={"class": "btn btn-fill btn-primary"}),
            acquisition_date=forms.DateInput(format=('%Y-%m-%d'),
                                             attrs={'class': 'form-control', 'placeholder': 'Select Date',
                                                    'type': 'date'}),

            expiration_date=forms.DateInput(format=('%Y-%m-%d'),
                                            attrs={"class": "form-control datepicker", "placeholder": "Select date",
                                                   "type": "date"}),
        )
