from http import HTTPStatus

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render, reverse

from .forms import ProductForm
from .models import Product


@login_required(login_url='/accounts/login')
def home(request):
    products = Product.objects.filter(owner=request.user).order_by('expiration_date')
    context = dict(products=products)
    return render(request, 'products/home.html', context)


@login_required(login_url='/accounts/login')
def new_product(request):
    product_form = ProductForm(request.POST or None, request.FILES or None)
    if request.method == 'POST':
        if product_form.is_valid():
            instance = product_form.save(commit=False)
            instance.owner = request.user
            instance.save()
            messages.success(request, "Produto salvo com sucesso!")
            return render(request, 'products/home.html', status=HTTPStatus.CREATED)

        context = dict(product_form=product_form)
        return render(request, 'products/new_products.html', context=context)

    else:
        context = dict(product_form=product_form)
        return render(request, 'products/new_product.html', context=context)


@login_required(login_url='/accounts/login')
def edit_product(request, product_id):
    if Product.objects.filter(owner=request.user, id=product_id).exists():
        product = Product.objects.get(owner=request.user, id=product_id)
        product_form = ProductForm(request.POST or None, instance=product)
        if request.method == "POST":
            if product_form.is_valid():
                product_form.save()
                messages.success(request, "Produto atualizado com sucesso!")
                return redirect(reverse('edit-product', args=[product_id]))

        else:
            context = dict(product_form=product_form)
            return render(request, 'products/edit_product.html', context=context, status=HTTPStatus.OK)

    else:
        messages.warning(request, 'Você não tem autorização para editar esse produto!')
        return render(request, 'products/home.html', status=HTTPStatus.UNAUTHORIZED)


@login_required(login_url='/accounts/login')
def delete_product(request, product_id):
    if request.method == "POST":
        Product.objects.filter(pk=product_id, owner=request.user).delete()
        messages.success(request, 'Produto deletado com sucesso!')
        return redirect('home')

    else:
        return redirect('home')
