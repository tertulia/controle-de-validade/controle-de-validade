import datetime
import tempfile
from http import HTTPStatus

from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

from apps.products.forms import ProductForm
from apps.products.models import Product


class TestHomeView(TestCase):
    def setUp(self) -> None:
        self.home_url = reverse('home')
        self.user = get_user_model().objects.create_user(email='user@email.com', name='user name', password='123')
        self.product = Product.objects.create(name='a thing',
                                              image=tempfile.NamedTemporaryFile(suffix='.jpg').name,
                                              description='a cool description',
                                              acquisition_date=datetime.date(2000, 1, 1),
                                              expiration_date=datetime.date(3000, 10, 1),
                                              voucher_image=tempfile.NamedTemporaryFile(suffix='.jpg').name,
                                              owner=self.user,
                                              )

        self.client = Client()

    def test_no_logged_user_redirect_to_index_page(self):
        response = self.client.get(self.home_url)
        self.assertRedirects(response, '/accounts/login?next=/home')

    def test_home_render_html(self):
        self.client.force_login(self.user)
        response = self.client.get(self.home_url)
        self.assertTemplateUsed(response, 'products/home.html')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'partials/_footer.html')
        self.assertTemplateUsed(response, 'partials/_sidebar.html')

    def test_products_user_is_in_context_home_page(self):
        self.client.force_login(self.user)
        response = self.client.get(self.home_url)
        self.assertIn(self.product, response.context['products'])


class TestNewProductView(TestCase):
    def setUp(self) -> None:
        self.new_product_url = reverse('new-product')
        self.user = get_user_model().objects.create_user(email='user@email.com', name='user name', password='123')
        self.client = Client()
        self.client.force_login(self.user)
        self.product_data = dict(name='thing',
                                 image=tempfile.NamedTemporaryFile(suffix='.png').name,
                                 description='a nice description about this product',
                                 acquisition_date=datetime.date(2020, 1, 1),
                                 expiration_date=datetime.datetime.today().date(),
                                 voucher_image=tempfile.NamedTemporaryFile(suffix='.jpeg').name,
                                 owner=self.user)

    def test_no_logged_user_redirects_to_index(self):
        no_logged_client = Client()
        response = no_logged_client.get(self.new_product_url)
        self.assertRedirects(response, '/accounts/login?next=/product/new-product')

    def test_no_post_product(self):
        response = self.client.get(self.new_product_url)
        self.assertTemplateUsed(response, 'products/new_product.html')

    def test_new_product_render_html(self):
        response = self.client.get(self.new_product_url)
        self.assertTemplateUsed(response, 'products/new_product.html')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'partials/_sidebar.html')
        self.assertTemplateUsed(response, 'partials/_footer.html')

    def test_post_end_create_product_success(self):
        response = self.client.post(self.new_product_url, self.product_data)
        self.product_data.pop('image')
        self.product_data.pop('voucher_image')
        self.assertEqual(response.status_code, HTTPStatus.CREATED)
        self.assertTrue(Product.objects.filter(**self.product_data).exists())

    def test_message_post_product_success(self):
        response = self.client.post(self.new_product_url, data=self.product_data)
        self.assertEqual(response.status_code, HTTPStatus.CREATED)

        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, "success")
        self.assertTrue("Produto salvo com sucesso!" in message.message)


class TestEditProductView(TestCase):
    def get_edit_product_url(self, product_id):
        return reverse('edit-product', args=[product_id])

    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user(email='user@email.com', name='user name', password='123')
        self.client = Client()
        self.client.force_login(self.user)
        self.product_data = dict(name='thing',
                                 image=tempfile.NamedTemporaryFile(suffix='.png').name,
                                 description='a nice description about this product',
                                 acquisition_date=datetime.date(2020, 1, 1),
                                 expiration_date=datetime.datetime.today().date(),
                                 voucher_image=tempfile.NamedTemporaryFile(suffix='.jpeg').name)

    def test_unauthenticated_access_edit_product(self):
        other_client = Client()
        response = other_client.get(self.get_edit_product_url(product_id=1))
        self.assertRedirects(response, '/accounts/login?next=/product/edit-product/1')

    def test_message_unauthorized_access_edit_product(self):
        response = self.client.get(self.get_edit_product_url(product_id=1))

        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, "warning")
        self.assertTrue("Você não tem autorização para editar esse produto!" in message.message)

    def test_owner_get_edit_product_page(self):
        self.product_data['owner'] = self.user
        product = Product.objects.create(**self.product_data)
        response = self.client.get(self.get_edit_product_url(product.id))
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_product_in_context(self):
        self.product_data['owner'] = self.user
        product = Product.objects.create(**self.product_data)
        product_form = ProductForm(instance=product)
        response = self.client.get(self.get_edit_product_url(product.id))
        self.assertEqual(response.context['product_form'].initial, product_form.initial)

    def test_edit_product_success(self):
        product = Product.objects.create(**self.product_data, owner=self.user)
        self.product_data['name'] = 'new name'
        self.client.post(self.get_edit_product_url(product.id), data=self.product_data)
        product.refresh_from_db()
        self.assertEqual(product.name, self.product_data['name'])


class TestDeleteProductView(TestCase):
    def get_delete_url(self, product_id):
        return reverse('delete-product', args=[product_id])

    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user(email='user@email.com', name='user name', password='123')
        self.client = Client()
        self.client.force_login(self.user)
        self.product_data = dict(name='thing',
                                 image=tempfile.NamedTemporaryFile(suffix='.png').name,
                                 description='a nice description about this product',
                                 acquisition_date=datetime.date(2020, 1, 1),
                                 expiration_date=datetime.datetime.today().date(),
                                 voucher_image=tempfile.NamedTemporaryFile(suffix='.jpeg').name,
                                 owner=self.user)
        self.product = Product.objects.create(**self.product_data)

    def test_unauthenticated_client(self):
        unauthenticated_client = Client()
        response = unauthenticated_client.get(self.get_delete_url(product_id=self.product.id))
        self.assertRedirects(response, '/accounts/login?next=/product/delete-product/1')

    def test_unauthorized_client(self):
        unauthorized_client = Client()
        response = unauthorized_client.get(self.get_delete_url(product_id=self.product.id))
        self.assertRedirects(response, '/accounts/login?next=/product/delete-product/1')

    def test_delete_success(self):
        response = self.client.post(self.get_delete_url(self.product.id))
        product = Product.objects.filter(pk=self.product.id)
        self.assertFalse(product.exists())
        self.assertRedirects(response, '/home')
