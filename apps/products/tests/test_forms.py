import datetime
import tempfile

from django.contrib.auth import get_user_model
from django.test import TestCase

from ..forms import ProductForm


class TestProductForm(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user(email='user@email.com', name='user name', password='123')
        self.product_data = dict(name='thing',
                                 image=tempfile.NamedTemporaryFile(suffix='.png').name,
                                 description='a nice description about this product',
                                 acquisition_date=datetime.date(2020, 1, 1),
                                 expiration_date=datetime.datetime.today().date(),
                                 voucher_image=tempfile.NamedTemporaryFile(suffix='.jpeg').name,
                                 owner=self.user)

    def test_product_valid_data(self):
        form = ProductForm(data=self.product_data)
        self.assertTrue(form.is_valid())

    def test_product_form_no_data(self):
        form = ProductForm(data={})
        self.assertFalse(form.is_valid())
