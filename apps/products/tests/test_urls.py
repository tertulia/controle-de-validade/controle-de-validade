from django.test import TestCase
from django.urls import reverse, resolve

from .. import views


class TestProductsUrls(TestCase):
    def test_home_url(self):
        url = reverse('home')
        self.assertEqual(resolve(url).func, views.home)
        self.assertEqual(url, '/home')

    def test_register_product_url(self):
        url = reverse('new-product')
        self.assertEqual(resolve(url).func, views.new_product)
        self.assertEqual(url, '/product/new-product')

    def test_edit_product_url(self):
        url = reverse('edit-product', args=[1])
        self.assertEqual(resolve(url).func, views.edit_product)
        self.assertEqual(url, '/product/edit-product/1')

    def test_delete_product_url(self):
        url = reverse('delete-product', args=[1])
        self.assertEqual(resolve(url).func, views.delete_product)
        self.assertEqual(url, '/product/delete-product/1')
