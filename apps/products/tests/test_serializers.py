import datetime

from django.contrib.auth import get_user_model
from django.test import TestCase

from ..api.serializers import ProductSerializer
from ..models import Product


class TestProductSerializer(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user(email='user@email.com', name='user name', password='pass')
        self.product_params = dict(name='some product', description='stuff',
                                   acquisition_date=datetime.datetime.now().date(),
                                   expiration_date=datetime.timedelta(31) + datetime.datetime.now().date()
                                   )
        self.product = Product.objects.create(owner=self.user, **self.product_params)

    def test_expected_fields(self):
        expected_fields = {'id', 'name', 'image', 'description', 'acquisition_date', 'expiration_date', 'voucher_image',
                           'status', 'owner'}
        serializer = ProductSerializer(self.product)

        self.assertEqual(set(serializer.data.keys()), expected_fields)

    def test_fields_content(self):
        serializer = ProductSerializer(self.product)
        self.product_params['acquisition_date'] = str(self.product_params['acquisition_date'])
        self.product_params['expiration_date'] = str(self.product_params['expiration_date'])
        self.product_params['owner'] = self.user.id
        self.product_params['status'] = self.product.get_new_status()
        self.product_params['voucher_image'] = None
        self.product_params['image'] = None
        self.product_params['id'] = self.product.id

        self.assertDictEqual(serializer.data, self.product_params)
