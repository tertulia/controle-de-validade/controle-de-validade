import datetime
import os
import tempfile

from PIL import Image
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from ..api.serializers import ProductSerializer
from ..models import Product

URL_PRODUCT = reverse('products-list')


def sample_user(email='user@meail.com', name='user name', password='pass'):
    return get_user_model().objects.create_user(email, name, password)


def sample_product(owner, name='product name', description='a thing',
                   acquisition_date=datetime.datetime.now().date(),
                   expiration_date=datetime.timedelta(31) + datetime.datetime.now().date()):
    return Product.objects.create(owner=owner, name=name, description=description,
                                  acquisition_date=acquisition_date, expiration_date=expiration_date)


class TestPublicProductApi(TestCase):
    def setUp(self) -> None:
        self.user = sample_user()
        self.client = APIClient()
        self.client.force_authenticate(self.user)
        self.product_params = dict(name='some product', description='stuff',
                                   acquisition_date=datetime.datetime.now().date(),
                                   expiration_date=datetime.timedelta(31) + datetime.datetime.now().date()
                                   )

    def test_list_retrieve_owners_product(self):
        sample_product(self.user)
        sample_product(self.user)
        sample_product(self.user)

        other_user = sample_user('other_user@email.com')
        sample_product(other_user)
        sample_product(other_user)
        sample_product(other_user)

        products = Product.objects.filter(owner=self.user)
        serializer = ProductSerializer(products, many=True)

        response = self.client.get(URL_PRODUCT)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_create_product_success(self):
        self.product_params['expiration_date'] = str(self.product_params['expiration_date'])
        self.product_params['acquisition_date'] = str(self.product_params['acquisition_date'])
        response = self.client.post(URL_PRODUCT, self.product_params)

        product = Product.objects.get(id=response.data['id'])
        serializer = ProductSerializer(product)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, serializer.data)

    def test_create_product_fail(self):
        self.product_params['expiration_date'] = str(self.product_params['expiration_date'])
        self.product_params['acquisition_date'] = str(self.product_params['acquisition_date'])

        for key, value in self.product_params.copy().items():
            self.product_params.pop(key)
            response = self.client.post(URL_PRODUCT, self.product_params)

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_product_by_owner_success(self):
        product = sample_product(self.user, **self.product_params)

        payload = dict(name='new product name', description='other description',
                       acquisition_date=str(datetime.timedelta(31) + datetime.datetime.now().date()),
                       expiration_date=str(datetime.timedelta(60) + datetime.datetime.now().date())
                       )

        response = self.client.patch(reverse('products-detail', args=[product.id]), payload)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], payload['name'])
        self.assertEqual(response.data['description'], payload['description'])
        self.assertEqual(response.data['acquisition_date'], payload['acquisition_date'])
        self.assertEqual(response.data['expiration_date'], payload['expiration_date'])

    def test_update_product_other_owner(self):
        other_user = sample_user('other@email.com')
        product = sample_product(other_user)

        payload = dict(name='new name')
        response = self.client.patch(reverse('products-detail', args=[product.id]), payload)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_deletion_of_product_by_owner(self):
        product = sample_product(self.user)
        response = self.client.delete(reverse('products-detail', args=[product.id]))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_deletion_of_product_by_other_user(self):
        other_user = sample_user('other@email.com')
        product = sample_product(other_user)
        response = self.client.delete(reverse('products-detail', args=[product.id]))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_methods_not_allowed(self):
        product = sample_product(self.user)

        response = self.client.put(reverse('products-detail', args=[product.id]), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class ProductImageUploadTests(TestCase):

    def setUp(self):
        self.user = sample_user()
        self.client = APIClient()
        self.client.force_authenticate(self.user)
        self.product = sample_product(self.user)

    def tearDown(self):
        self.product.image.delete()
        self.product.voucher_image.delete()

    def test_upload_image_to_product(self):
        """Test uploading an image to product"""
        url = reverse('products-detail', args=[self.product.id])
        with tempfile.NamedTemporaryFile(suffix='.jpg') as ntf:
            img = Image.new('RGB', (10, 10))
            img.save(ntf, format='JPEG')
            ntf.seek(0)
            res = self.client.patch(url, {'image': ntf}, format='multipart')

        self.product.refresh_from_db()
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertIn('image', res.data)
        self.assertTrue(os.path.exists(self.product.image.path))

    def test_upload_voucher_image_to_product(self):
        """Test uploading an voucher image to product"""
        url = reverse('products-detail', args=[self.product.id])
        with tempfile.NamedTemporaryFile(suffix='.jpg') as ntf:
            img = Image.new('RGB', (10, 10))
            img.save(ntf, format='JPEG')
            ntf.seek(0)
            res = self.client.patch(url, {'voucher_image': ntf}, format='multipart')

        self.product.refresh_from_db()
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertIn('voucher_image', res.data)
        self.assertTrue(os.path.exists(self.product.voucher_image.path))

    def test_upload_image_bad_request(self):
        """Test uploading an invalid image"""
        url = reverse('products-detail', args=[self.product.id])
        res = self.client.patch(url, {'image': 'notimage'}, format='multipart')

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_upload_voucher_image_bad_request(self):
        """Test uploading an invalid image"""
        url = reverse('products-detail', args=[self.product.id])
        res = self.client.patch(url, {'voucher_image': 'notimage'}, format='multipart')

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)


class TestProductPath(TestCase):
    def test_path_in_products_routes(self):
        list_url = '/api/products/'
        self.assertEqual(URL_PRODUCT, list_url)

        detail_url = '/api/products/1/'
        self.assertEqual(reverse('products-detail', args=[1]), detail_url)
