import datetime
from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.test import TestCase

from ..models import Product, product_image_file_path


class TestProductModel(TestCase):
    def setUp(self) -> None:
        self.staus_product = dict(great='GR',
                                  good='GO',
                                  almost='AL',
                                  expired='EX')
        self.owner = get_user_model().objects.create_user('user@email.com', 'user name', 'some pass')
        self.product_params = dict(name='something', description='a thing I have',
                                   acquisition_date=datetime.datetime.now().date(),
                                   expiration_date=datetime.timedelta(31) + datetime.datetime.now().date(),
                                   owner=self.owner)

    def test_create_product_success(self):
        product = Product.objects.create(**self.product_params)

        self.assertEqual(product.name, self.product_params['name'])
        self.assertEqual(product.description, self.product_params['description'])
        self.assertEqual(product.acquisition_date, self.product_params['acquisition_date'])
        self.assertEqual(product.expiration_date, self.product_params['expiration_date'])
        self.assertEqual(product.owner, self.product_params['owner'])

    def test_representation_of_product(self):
        product = Product.objects.create(**self.product_params)
        self.assertEqual(str(product), product.name)

    def test_save_status_properly(self):
        today = datetime.datetime.today().date()

        self.product_params['expiration_date'] = today
        product = Product.objects.create(**self.product_params)
        self.assertEqual(product.status, self.staus_product['expired'])

        product.expiration_date = datetime.timedelta(7) + today
        product.save()
        self.assertEqual(product.status, self.staus_product['almost'])

        product.expiration_date = datetime.timedelta(30) + today
        product.save()
        self.assertEqual(product.status, self.staus_product['good'])

        product.expiration_date = datetime.timedelta(31) + today
        product.save()
        self.assertEqual(product.status, self.staus_product['great'])

    def test_get_new_status_of_product(self):
        product = Product.objects.create(**self.product_params)
        self.assertEqual(product.status, product.get_new_status())

        product.expiration_date = datetime.timedelta(7) + datetime.datetime.today().date()
        product.update_status()
        self.assertEqual(product.status, product.get_new_status())
        self.assertEqual(self.staus_product['almost'], product.get_new_status())

    def test_deletion_cascade(self):
        product = Product.objects.create(**self.product_params)
        self.owner.delete()

        product_exists = Product.objects.filter(id=product.id).exists()
        self.assertFalse(product_exists)

    @patch('uuid.uuid4')
    def test_product_file_name_uuid(self, mock_uuid):
        """Test that image is saved in the correct location"""
        uuid = 'test-uuid'
        mock_uuid.return_value = uuid
        file_path = product_image_file_path(None, 'product_image.jpg')

        expected_path = f"uploads/product/{uuid}.jpg"
        self.assertEqual(file_path, expected_path)
