from django.urls import path

from . import views

urlpatterns = [
    path('home', views.home, name='home'),
    path('product/new-product', views.new_product, name='new-product'),
    path('product/edit-product/<int:product_id>', views.edit_product, name='edit-product'),
    path('product/delete-product/<int:product_id>', views.delete_product, name='delete-product'),
]
