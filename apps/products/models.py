import datetime as dt
import uuid
import os
from django.conf import settings
from django.db import models


def product_image_file_path(instance, filename):
    """Generate file path for new product image"""
    extension = filename.split('.')[-1]
    filename = f"{uuid.uuid4()}.{extension}"

    return os.path.join('uploads/product/', filename)


class Product(models.Model):
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to=product_image_file_path, null=True, blank=True)
    description = models.CharField(max_length=100, null=True, blank=True)
    acquisition_date = models.DateField()
    expiration_date = models.DateField()
    voucher_image = models.ImageField(upload_to=product_image_file_path, null=True, blank=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    great = 'GR'
    good = 'GO'
    almost = 'AL'
    expired = 'EX'
    STATUS = (
        (great, 'great'),
        (good, 'good'),
        (almost, 'almost'),
        (expired, 'expired'),
    )
    status = models.CharField(max_length=2, choices=STATUS, null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None, *args, **kwargs):
        self.status = self.get_new_status()
        super(Product, self).save(*args, **kwargs)

    def get_new_status(self):
        today = dt.datetime.today().date()
        expired = dt.timedelta(0)
        one_week = dt.timedelta(7)
        one_month = dt.timedelta(30)
        days_to_expire = self.expiration_date - today

        if days_to_expire <= expired:
            return 'EX'
        elif days_to_expire <= one_week:
            return 'AL'
        elif days_to_expire <= one_month:
            return 'GO'
        return 'GR'

    def update_status(self):
        self.save(update_fields=['status'])
