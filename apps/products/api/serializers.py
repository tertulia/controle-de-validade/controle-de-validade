from rest_framework.serializers import ModelSerializer

from apps.products.models import Product


class ProductSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'name', 'image', 'description',
                  'acquisition_date', 'expiration_date', 'voucher_image',
                  'status', 'owner']
        extra_kwargs = {"owner": {"read_only": True}}
