from rest_framework import viewsets, mixins
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from .permissions import UpdateOwnProducts
from .serializers import ProductSerializer
from ..models import Product


class ProductViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin, mixins.UpdateModelMixin,
                     mixins.DestroyModelMixin):
    """Handles creating, reading and updating MyUsers products"""
    authentication_classes = (TokenAuthentication,)
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    permission_classes = (UpdateOwnProducts, IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        """Return only the products of the owner"""
        queryset = self.queryset.filter(owner=self.request.user)
        return queryset
