from rest_framework import serializers
from rest_framework.authtoken.models import Token

from ..models import MyUser


class MyUserSerializer(serializers.ModelSerializer):
    """Serializers a my_user object"""
    token = serializers.SerializerMethodField()

    class Meta:
        model = MyUser
        fields = ["email", "name", "id", "password", 'token']
        extra_kwargs = {
            'password': {
                'write_only': True,
                'style': {'input_type': 'password'}
            },
            'token': {
                'read_only': True,
            }
        }

    def get_token(self, obj):
        token, _ = Token.objects.get_or_create(user=obj)
        return token.key

    def create(self, validated_data):
        """Create and return a new user"""
        user = MyUser.objects.create_user(
            email=validated_data['email'],
            name=validated_data['name'],
            password=validated_data['password'],
        )
        Token.objects.get_or_create(user=user)
        return user

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance
