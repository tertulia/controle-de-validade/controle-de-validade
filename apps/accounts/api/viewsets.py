from django.contrib.auth import get_user_model, authenticate
from rest_framework import viewsets, mixins, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from . import permissions
from .serializers import MyUserSerializer


class MyUserViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin):
    """Handle creating and updating profiles"""
    serializer_class = MyUserSerializer
    queryset = get_user_model().objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, permissions.UpdateOwnUser,)

    @action(methods=['post', ], detail=False, permission_classes=[], authentication_classes=[])
    def register(self, request, *args, **kwargs):
        is_missing, value = missing_field(request.data, 'password', 'name', 'email')
        if is_missing:
            return Response({'message': f"field {value} is required"}, status=status.HTTP_400_BAD_REQUEST)

        if get_user_model().objects.filter(email=request.data['email']).exists():
            return Response({"message": "email already exists"}, status=status.HTTP_409_CONFLICT)

        return self.create(request, *args, **kwargs)

    @action(methods=['post', ], detail=False, permission_classes=[], authentication_classes=[])
    def login(self, request, *args, **kwargs):
        is_missing, value = missing_field(request.data, 'email', 'password')
        if is_missing:
            return Response({'message': f'field {value} is required'}, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(email=request.data['email'],
                            password=request.data['password'])
        if user:
            serializer = self.get_serializer(user)
            return Response(serializer.data)

        return Response({'message': "invalid email or password"},
                        status=status.HTTP_400_BAD_REQUEST)


def missing_field(data, *fields):
    for field in fields:
        value = data.get(field, None)
        if not value:
            return True, field
    return False, None
