from http import HTTPStatus

from django.contrib import auth, messages
from django.contrib.auth import get_user_model
from django.shortcuts import render, redirect


def login(request):
    if request.user.is_authenticated:
        return redirect('home')
    elif request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']

        user = auth.authenticate(email=email, password=password)

        if user:
            auth.login(request, user)
            messages.success(request, "Você está logado")
            return render(request, 'products/home.html', status=HTTPStatus.CREATED)
        else:
            messages.warning(request, "Credenciais inválidas")
            return render(request, "accounts/login.html", status=HTTPStatus.BAD_REQUEST)

    return render(request, 'accounts/login.html', status=HTTPStatus.OK)


def register(request):
    if request.user.is_authenticated:
        return redirect('home')

    elif request.method == "POST":
        email = request.POST['email']
        name = request.POST['name']
        password = request.POST['password']
        password2 = request.POST['password2']

        if password == password2:
            if get_user_model().objects.filter(email=email).exists():
                messages.warning(request, "Email já cadastrado")
                return render(request, 'accounts/register.html', status=HTTPStatus.BAD_REQUEST)
            else:
                user = get_user_model().objects.create_user(name=name, email=email, password=password)

                auth.login(request, user)
                messages.success(request, "Você está logado agora")
                return render(request, 'products/home.html', status=HTTPStatus.CREATED)

        else:
            messages.warning(request, 'Senhas não são iguais')
            return render(request, 'accounts/register.html', status=HTTPStatus.BAD_REQUEST)

    return render(request, 'accounts/register.html')


def logout(request):
    auth.logout(request)
    messages.success(request, "Você está deslogado!")
    return redirect('index')
