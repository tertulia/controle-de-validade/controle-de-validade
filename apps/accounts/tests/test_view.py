from http import HTTPStatus

from django.contrib import auth
from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse


class TestLoginView(TestCase):
    def setUp(self) -> None:
        self.login_url = reverse('login')
        self.data_user = dict(email='user@email.com', name='a cool name', password='some pass')
        self.user = get_user_model().objects.create_user(**self.data_user)
        self.client = Client()

    def test_login_render_html(self):
        response = self.client.get(self.login_url)
        self.assertTemplateUsed(response, 'accounts/login.html')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'partials/_footer.html')
        self.assertTemplateUsed(response, 'partials/_navbar.html')
        self.assertTemplateNotUsed(response, 'partials/_sidebar.html')

    def test_login_user_successful(self):
        response = self.client.post(self.login_url, data=self.data_user)
        self.assertEqual(response.status_code, HTTPStatus.CREATED)
        self.assertTemplateUsed(response, 'products/home.html')

    def test_login_invalid_credentials(self):
        response = self.client.post(self.login_url, data={"email": "not_registered@user.com", "password": "1234"})
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, "warning")
        self.assertTrue("Credenciais inválidas" in message.message)

    def test_logged_user_redirects_to_home(self):
        self.client.force_login(self.user)
        response = self.client.post(self.login_url, {})
        self.assertRedirects(response, '/home')


class TestRegisterView(TestCase):
    def setUp(self) -> None:
        self.register_url = reverse('register')
        self.data_user = dict(email='user@email.com', name='a cool name', password='some pass')
        self.client = Client()

    def test_register_render_html(self):
        response = self.client.get(self.register_url)
        self.assertTemplateUsed(response, 'accounts/register.html')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'partials/_footer.html')
        self.assertTemplateUsed(response, 'partials/_navbar.html')

    def test_register_success(self):
        self.data_user['password2'] = self.data_user['password']
        response = self.client.post(self.register_url, data=self.data_user)
        self.assertEqual(response.status_code, HTTPStatus.CREATED)
        self.assertTemplateUsed(response, 'products/home.html')

        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, "success")
        self.assertTrue("Você está logado agora" in message.message)

    def test_register_fail_diff_passwords(self):
        self.data_user['password2'] = 'not same pass'
        response = self.client.post(self.register_url, data=self.data_user)
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)
        self.assertTemplateUsed(response, 'accounts/register.html')

        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, "warning")
        self.assertTrue("Senhas não são iguais" in message.message)

    def test_register_fail_email_exists(self):
        get_user_model().objects.create_user(**self.data_user)
        self.data_user['password2'] = self.data_user['password']
        response = self.client.post(self.register_url, data=self.data_user)
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)
        self.assertTemplateUsed(response, 'accounts/register.html')

    def test_user_already_authenticated_redirect_to_home(self):
        user = get_user_model().objects.create_user(**self.data_user)
        self.client.force_login(user)
        response = self.client.post(self.register_url, {})
        self.assertRedirects(response, '/home')


class TestLogouView(TestCase):
    def setUp(self) -> None:
        self.logout_url = reverse('logout')
        self.user = get_user_model().objects.create_user(email='user@email.com', name='a cool name',
                                                         password='some pass')
        self.client = Client()
        self.client.force_login(self.user)

    def test_user_is_de_authenticate(self):
        self.client.get(self.logout_url)
        user = auth.get_user(self.client)
        self.assertFalse(user.is_authenticated)

    def test_logout_redirect(self):
        response = self.client.get(self.logout_url)
        self.assertRedirects(response, '/')
