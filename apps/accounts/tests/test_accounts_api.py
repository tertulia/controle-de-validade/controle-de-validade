from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from apps.accounts.api.viewsets import missing_field

URL_REGISTER_USER = reverse('accounts-register')
URL_LOGIN_USER = reverse('accounts-login')


def sample_user(**params):
    return get_user_model().objects.create_user(**params)


class TestsPublicUserApi(TestCase):
    def setUp(self) -> None:
        self.user_params = dict(email='user@email.com', name='user name', password='pass')
        self.client = APIClient()

    def test_create_valid_user_success(self):
        response = self.client.post(URL_REGISTER_USER, self.user_params)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        user = get_user_model().objects.get(email=self.user_params['email'])
        self.assertTrue(user.check_password(self.user_params['password']))
        self.assertNotIn('password', response.data)
        self.assertIn('token', response.data)

    def test_user_email_exists(self):
        response = self.client.post(URL_REGISTER_USER, self.user_params)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.post(URL_REGISTER_USER, self.user_params)
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        self.assertEqual(response.data['message'], 'email already exists')

    def test_create_user_fail_missing_field(self):
        email = 'user@email.com'
        name = 'user name'
        password = 'some pass'

        response = self.client.post(URL_REGISTER_USER, dict(email=email, password=password))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('name', response.data['message'])

        response = self.client.post(URL_REGISTER_USER, dict(name=name, password=password))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('email', response.data['message'])

        response = self.client.post(URL_REGISTER_USER, dict(email=email, name=name))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('password', response.data['message'])

    def test_login_user_success(self):
        self.client.post(URL_REGISTER_USER, self.user_params)

        response = self.client.post(URL_LOGIN_USER, dict(email=self.user_params['email'],
                                                         password=self.user_params['password']))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_login_user_fail(self):
        self.client.post(URL_REGISTER_USER, self.user_params)

        response = self.client.post(URL_LOGIN_USER, dict(email=self.user_params['email']))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], 'field password is required')

        response = self.client.post(URL_LOGIN_USER, dict(password=self.user_params['password']))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], 'field email is required')

        response = self.client.post(URL_LOGIN_USER, dict(email=self.user_params['email'],
                                                         password='other pass'))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], 'invalid email or password')

        response = self.client.post(URL_LOGIN_USER, dict(password=self.user_params['password'],
                                                         email='other@email.com'))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], 'invalid email or password')


class TestPrivateUserApi(TestCase):
    def setUp(self) -> None:
        self.user = sample_user(email='user@email.com', name='user name', password='pass')
        self.client = APIClient()
        self.client.force_authenticate(self.user)

    def test_update_user_account(self):
        payload = dict(email='new@email.com', name='new name', password='new pass')

        response = self.client.patch(reverse('accounts-detail', args=[self.user.id]), payload)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.user.refresh_from_db()
        self.assertEqual(self.user.name, payload['name'])
        self.assertEqual(self.user.email, payload['email'])
        self.assertTrue(self.user.check_password(payload['password']))

    def test_update_other_user_account(self):
        other_user = sample_user(email='other@email.com', name='other name', password='pass')
        other_client = APIClient()
        other_client.force_authenticate(other_user)

        payload = dict(email='new@email.com', name='new name', password='new pass')

        response = other_client.patch(reverse('accounts-detail', args=[self.user.id]), payload)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class TestAccountsPath(TestCase):
    def test_path_in_accounts_routes(self):
        register_url = '/api/accounts/register/'
        self.assertEqual(URL_REGISTER_USER, register_url)

        login_url = '/api/accounts/login/'
        self.assertEqual(URL_LOGIN_USER, login_url)

        update_url = '/api/accounts/1/'
        self.assertEqual(reverse('accounts-detail', args=[1]), update_url)


class TestUtilsApi(TestCase):
    def test_missing_field_function(self):
        data = dict(foo='foo', bar='bar', lorem='lorem', ipsum='ipsum')

        for key, value in data.copy().items():
            is_missing, missing_value = missing_field(data, key)
            self.assertFalse(is_missing)
            self.assertIsNone(missing_value)

            data.pop(key)
            is_missing, missing_value = missing_field(data, key)
            self.assertTrue(is_missing)
            self.assertEqual(key, missing_value)
