from django.contrib.auth import get_user_model
from django.test import TestCase


class TestMyUserModel(TestCase):
    def setUp(self) -> None:
        self.user_params = dict(name='user name',
                                email='user@email.com',
                                password='some pass')

    def test_create_my_user_with_success(self):
        """Test creating a new user is successful"""
        user = get_user_model().objects.create_user(**self.user_params)

        self.assertEqual(user.email, self.user_params['email'],
                         f"user.email must be {self.user_params['email']}")
        self.assertTrue(user.check_password(self.user_params['password']))

    def test_new_user_email_normalized(self):
        """Test the email for a new user is normalized"""
        email = 'user@SoMeEmAIl.cOM'
        user = get_user_model().objects.create_user(email=email,
                                                    name=self.user_params['name'],
                                                    password=self.user_params['password'])
        self.assertEqual(user.email, email.lower(),
                         'user.email must be normalized')

    def test_new_invalid_email(self):
        """Test creating a new user with no email raise error"""
        with self.assertRaises(ValueError):
            self.user_params.pop('email')
            get_user_model().objects.create_user(None, **self.user_params)

    def test_new_invalid_name(self):
        """Test creating a new user with no name raise error"""
        with self.assertRaises(ValueError):
            self.user_params.pop('name')
            get_user_model().objects.create_user(name=None, **self.user_params)

    def test_user_with_existing_email(self):
        get_user_model().objects.create_user(**self.user_params)
        with self.assertRaises(Exception) as context:
            get_user_model().objects.create_user(**self.user_params)
        self.assertTrue("UNIQUE constraint failed: accounts_myuser.email" in str(context.exception),
                        'must rise the email must be unique')

    def test_get_full_name(self):
        user = get_user_model().objects.create_user(**self.user_params)
        self.assertEqual(user.name, user.get_full_name())

    def test_get_short_name(self):
        user = get_user_model().objects.create_user(**self.user_params)
        first_name, last_name, = user.name.split(' ')
        self.assertEqual(first_name, user.get_short_name())

    def test_representation_user(self):
        user = get_user_model().objects.create_user(**self.user_params)
        self.assertEqual(str(user), user.email)


class SuperMyUserModelTests(TestCase):
    def test_create_superuser(self):
        """Test creating a new superuser"""
        superuser = get_user_model().objects.create_superuser(
            email='super@email.com',
            name='super name',
            password='super pass'
        )

        self.assertTrue(superuser.is_superuser, 'must return True for a superuser')
        self.assertTrue(superuser.is_staff, 'must return True for a superuser')
