from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework.authtoken.models import Token

from apps.accounts.api.serializers import MyUserSerializer


def sample_user(**params):
    return get_user_model().objects.create_user(**params)


class TestMyUserSerializer(TestCase):
    def setUp(self) -> None:
        self.user_params = dict(email='user@email.com', name='user name', password='some pass')
        self.user = sample_user(**self.user_params)

    def test_expected_fields(self):
        expected_fields = {'name', 'email', 'token', 'id', }
        serializer = MyUserSerializer(self.user)

        self.assertSetEqual(set(serializer.data.keys()), expected_fields)

    def test_fields_content(self):
        serializer = MyUserSerializer(self.user)
        self.user_params.pop('password')

        token, _ = Token.objects.get_or_create(user=self.user)
        self.user_params['id'] = self.user.id
        self.user_params['token'] = token.key

        self.assertDictEqual(serializer.data, self.user_params)
