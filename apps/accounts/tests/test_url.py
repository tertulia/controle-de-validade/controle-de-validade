from django.test import TestCase
from django.urls import reverse, resolve

from .. import views


class TestAccountsUrls(TestCase):
    def test_login_url(self):
        url = reverse('login')
        self.assertEqual(resolve(url).func, views.login)
        self.assertEqual(url, '/accounts/login')

    def test_register_url(self):
        url = reverse('register')
        self.assertEqual(resolve(url).func, views.register)
        self.assertEqual(url, '/accounts/register')

    def test_logout_url(self):
        url = reverse('logout')
        self.assertEqual(resolve(url).func, views.logout)
        self.assertEqual(url, '/accounts/logout')
